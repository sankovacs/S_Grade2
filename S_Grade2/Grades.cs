﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S_Grade2
{
    public class Grades
    {
        private readonly string name;
        private readonly int grade;

        public Grades(string name, int grade)
        {
            this.name = name;
            this.grade = grade;
        }

        public string Name => this.name;
        public int Grade => this.grade;

        public override string ToString() => $"{this.Name}, {this.grade}";
        public static Grades operator +(Grades first, Grades second) => new Grades(second.Name, first.Grade + second.Grade);
        public static Grades operator /(Grades grade, int weight) => new Grades(grade.Name, grade.Grade/weight); 
        public static Grades operator +(Grades grade, int add) => new Grades(grade.Name, grade.Grade+add);
    }


   

}
