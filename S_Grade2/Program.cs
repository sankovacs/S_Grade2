﻿using System;
using System.Collections.Generic;
using System.IO;

namespace S_Grade2
{
    class Program
    {


        static void Main(string[] args)
        {
            using (var firstReader = new StreamReader(File.OpenRead(@"c:\Users\Sanyi\Documents\Visual Studio 2017\Projects\S_CalculateGrade\FistGrades.txt")))
            using (var secondReader = new StreamReader(File.OpenRead(@"c:\Users\Sanyi\Documents\Visual Studio 2017\Projects\S_CalculateGrade\SecondGrades.txt")))
            using (var averageWriter = new StreamWriter(File.Open(@"c:\Users\Sanyi\Documents\Visual Studio 2017\Projects\S_CalculateGrade\average.txt", FileMode.Create, FileAccess.Write)))

                foreach (var students in Roster(firstReader))
                {
                    averageWriter.WriteLine(GenerateAverage(students, firstReader.ReadGrades(), secondReader.ReadGrades()).ToString());
                }

        }
        public static IEnumerable<string> Roster(StreamReader reader)
        {
            var temp = new List<string>();
            string line;
            while ((line = reader.ReadLine()) != null)
            {
                var name = line.Split(' ')[0];
                if (!temp.Contains(name))
                {
                    temp.Add(name);
                }
            }
            return temp;
        }

        public static IEnumerable<Grades> GenerateAverage(string students, IEnumerable<Grades> first, IEnumerable<Grades> second)
        {
            var enumeratorFirst = first.GetEnumerator();
            var enumeratorSecond = second.GetEnumerator();
            bool hasMoreFirstGrade = true;
            bool hasMoreSecondGrade = true;
            Grades current = new Grades(null, 0);
            int weight = 0;

            while (hasMoreFirstGrade && String.Compare(students, enumeratorFirst.Current.Name) > 0)
                hasMoreFirstGrade = enumeratorFirst.MoveNext();
            
            while (hasMoreFirstGrade && String.Compare(students, enumeratorFirst.Current.Name) == 0)
            {
                current += enumeratorFirst.Current;
                weight++;
                hasMoreFirstGrade = enumeratorFirst.MoveNext();
            }

            while (hasMoreSecondGrade && String.Compare(students, enumeratorSecond.Current.Name) > 0)
                hasMoreSecondGrade = enumeratorFirst.MoveNext();

            while (hasMoreSecondGrade && String.Compare(students, enumeratorSecond.Current.Name) == 0)
            {
                current += enumeratorFirst.Current;
                weight++;
                hasMoreSecondGrade = enumeratorFirst.MoveNext();
            }

            yield return (current+((weight)/2)) / weight;

        }
    }
}