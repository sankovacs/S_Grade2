﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.IO;


namespace S_Grade2
{
    public static class GradeExtensions
    {
        public static IEnumerable<Grades> ReadGrades(this StreamReader reader) => reader.ReadGrade((name, grade) => new Grades(name, grade));
        
        public static IEnumerable <Grades> ReadGrade(this StreamReader reader, Func<string, int, Grades> generator)
           
        {
            string line;

            while ((line = reader.ReadLine()) != null)
            {
                var parts = line.Split(' ');

                yield return generator(parts[0], int.Parse(parts[1]));
            }
        }

       
    }
}